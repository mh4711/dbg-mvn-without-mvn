A simple way to convert a maven project to a "maven free" project. 
------------------------------------------------------------------ 

The following instructions show how to take a maven-generated project 
(for example, as given on https://vaadin.com/maven#archetypes) and 
convert it to an eclipse project that does not use maven. The resulting 
project can then be more easily debugged than a corresponding maven 
project. For example, changing a jar file or temporarily modifying a 
source file during a debugging session can be painful with a maven 
project. With a simple eclipse project it's relatively painless. 

1. start up the project as you normally would, execept add 
'-verbose:class' to the JVM options. Also, redirect the output to a 
file. For example, in my pom.xml I have: 

<jvmArgs>-verbose:class 
-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=1044</jvmArgs> 

From within the appropriate directory of your peoject run: 

mvn jetty:run-forked > run.out.txt 

2. Fire up the browser and use the app. Click on all the buttons and 
load all the dialogs, this is to ensure that the classes get loaded. Of 
course there is the possibility that you overlooked something. In most 
cases this should be quite rare and the classes' origins can be 
determined by inspection and the jar files can be linked into your 
eclipse project as needed. 

3. Modify the program debug_mvn_without_mvn.CopyJarsToOneDir 

Set the paths in the main function to the output file generated in (1) 
and another path to a directory where all the classpath jars will be 
copied. You may also need to examine which paths you don't want copied
and would rather setup by hand.

4. Run the program debug_mvn_without_mvn.CopyJarsToOneDir 

5. Create an eclipse project, and add all the jars from the target 
directory created in (3). If you would like to use the sources instead 
of the jar file, just don't import them into your project. For example, 
in my eclipse IDE I have imported Jetty 9.3.9 sources (generated from 
maven, no less) and so I don't import the Jetty jars copied from the 
Maven repo. 

6. Copy the fork.props file created in (1) to another directory since 
every time you re-run the app it will get overwritten. fork.props is 
created in the target subdirectory of the directory where the app was 
run from in (1) 

7. Modify two entries in fork.props 

# this should point to the output directory of your eclipse project 
classes.dir=C\:\\prj\\dbg-mvn-without-mvn\\bin 
# this should be empty 
lib.jars= 

The lib.jars should be empty, since all jars should already be available 
to the eclipse project created in (5). 

8. Modify the class debug_mvn_without_mvn.StarterWrapper to point to the 
"fork.props" file that was created by the run in step (1) above. 
Alternatively, you can create a launch configuration in eclipse and pass 
the paths as arguments. 

9. Launch debug_mvn_without_mvn.StarterWrapper 

this will call: org.eclipse.jetty.maven.plugin.Starter 

you will need the jetty-maven-plugin jar or project in your path. I have 
only had success with jetty-maven-plugin version 9.3.9. You may find it 
easier to build jetty from source so you can diagnose any problems should 
they occur.

10. Open your browser to localhost:8080 

Enjoy! 

