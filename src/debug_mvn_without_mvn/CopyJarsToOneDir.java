package debug_mvn_without_mvn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;

public class CopyJarsToOneDir {

	private static final String FROM_STRING = " from ";
	private static final String LOADED_STRING = "[Loaded ";
	private static final String[] ignorableAr = { //
			"java", //
			"sun", //
			"javax", //
//			"org.eclipse.persistence", //
//			"org.hibernate", //
//			"org.jboss", //
//			"org.postgresql", //
//			"org.jsoup", //
			"com.sun", //
			"org.xml.sax", //
			"jdk", //
			"org.jcp.xml", //
			"org.w3c", //
	};

	public static void main(String[] args) {
		File file = new File("C:\\prj\\bugrap_aid\\bugrap_aid-ui\\run.out.20160517.0922.txt");
		File expandPath = new File("C:\\prj\\bugrap-fromloaded");
		FileInputStream fis = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		Set<String> ignorable = new LinkedHashSet<>();
		Set<String> needsLoad = new LinkedHashSet<>();
		Set<String> expanded = new LinkedHashSet<>();
		Set<String> classDirs = new LinkedHashSet<>();
		ignorable.addAll(Arrays.asList(ignorableAr));
		try {
			fis = new FileInputStream(file);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			OUTER: for (String line = br.readLine(); line != null; line = br.readLine()) {
				int loadedIdx = line.indexOf(LOADED_STRING);
				if (loadedIdx >= 0) {
					int fromIdx = line.indexOf(FROM_STRING, loadedIdx + LOADED_STRING.length());
					if (fromIdx > 0) {
						String fromPath = line.substring(fromIdx + FROM_STRING.length());
						int lastRBracket = fromPath.lastIndexOf(']');
						if (lastRBracket > 0) {
							fromPath = fromPath.substring(0, lastRBracket);
						}
						String fqclass = line.substring(loadedIdx + LOADED_STRING.length(), fromIdx);
						int dotIdx = fqclass.indexOf('.');
						while (dotIdx > 0) {
							String package0 = fqclass.substring(0, dotIdx);
							dotIdx = fqclass.indexOf('.', dotIdx + 1);
							if (ignorable.contains(package0)) {
								// ignore;
								continue OUTER;
							}
						}
						l("class:[" + fqclass + "] from:[" + fromPath + "]");
						needsLoad.add(fqclass);
						if (!expanded.contains(fromPath) && !classDirs.contains(fromPath)) {
							if (fromPath.startsWith("C:\\Program Files\\Java\\jdk1.8.0_92")) {
								// ???
								l("funny path:" + fromPath);
							} else {

								expanded.add(fromPath);
								File jarFileFrom = new File(new URI(fromPath));
								if (jarFileFrom.isDirectory()) {
									// add to classfile dirs
									classDirs.add(fromPath);
								} else {
									File jarFileTo = new File(expandPath, jarFileFrom.getName());
									FileInputStream jarFis = null;
									FileOutputStream jarFos = null;
									try {
										jarFis = new FileInputStream(jarFileFrom);
										jarFos = new FileOutputStream(jarFileTo);
										IOUtils.copy(jarFis, jarFos);
									} finally {
										IOUtils.closeQuietly(jarFis);
										IOUtils.closeQuietly(jarFos);
									}
								}
							}
						}
					}
				}
			}
			l("needed:" + needsLoad.size() + " classes");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(br);
		}

	}

	private static void l(String s) {
		System.out.println(s);
	}

}
